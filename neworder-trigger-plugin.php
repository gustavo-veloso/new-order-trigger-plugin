<?php
/**
 * Plugin Name: New Order Trigger Plugin
 * Plugin URI: https://github.com/gustable42/neworder-trigger-plugin
 * Description: Sends an API Request every time a new Woocommerce Order is done
 * Version: 1.0
 * Author: Gustavo Veloso
 * Author URI: http://www.syscoin.com.br
 */

add_action( 'woocommerce_thankyou', 'custom_woocommerce_order_status_completed' );

function custom_woocommerce_order_status_completed( $order_id ) {
    $order = wc_get_order( $order_id );
    $order_data = $order->get_data();

    $total = $order_data['total'];
    $order_items = $order->get_items();
    $items_info = json_encode( get_products_info( $order_items ) );
    $store_url = get_site_url();

    $url = "http://us-central1-syscoin-dashboard-app.cloudfunctions.net/woocommerceNewOrderNotification?";
    $url .= "id=".$order_id;
    $url .= "&url=".$store_url;
    $url .= "&total=".$total;
    $url .= "&items=".$items_info;
    $http_request = file( $url );

    if( WP_DEBUG ){
        echo "<script> alert('".$order_id."'); </script>"; 
    }
}

function get_products_info( $order_items ) {
    $items_info = array(
        name => array(),
        quantity => array(),
        item_total => array()
    );

    foreach ($order_items as $item_id => $item_data) {
        $items_info['name'][] = $item_data['name'];
        $items_info['quantity'][] = $item_data['quantity'];
        $items_info['item_total'][] = $item_data['total'];
    }
    
    return $items_info;
}